import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_practice/TextLabel.dart';
import 'package:provider_practice/providers/text_provider.dart';

import './some_buttons.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider practice'),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Container(
                height: 300,
                child: Center(
                  child: TextLabel(),
                ),
              ),
            ],
          ),
          SomeButtons(),
        ],
      ),
    );
  }
}
