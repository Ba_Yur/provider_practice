import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/text_provider.dart';

class TextLabel extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    String text = Provider.of<TextProvider>(context).someText;
    return Text(text,
    style: TextStyle(
      fontSize: 30
    ),);
  }
}
