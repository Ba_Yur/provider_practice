import 'package:flutter/cupertino.dart';

class TextProvider with ChangeNotifier {
  String _someText = 'Initial text';

  String get someText {
    return _someText;
  }


  List<String> _listOfStrings = [
    'Text #1',
    'Text #2',
    'Text #3',
    'Text #4',
  ];

  List<String> get listOfStrings {
    return [..._listOfStrings];
  }

  void changeText (String value) {
    _someText = value;
    notifyListeners();
  }
}

