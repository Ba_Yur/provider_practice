import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/text_provider.dart';

class SomeButtons extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    TextProvider textProvider = Provider.of<TextProvider>(context, listen: false);
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [...textProvider.listOfStrings.map((value) {
            return ElevatedButton(
              onPressed: () {
                textProvider.changeText(value);
              },
              child: Text(value),
            );
          })
            // ElevatedButton(
            //   onPressed: () {
            //     textProvider.changeText('Text #1');
            //   },
            //   child: Text('Text #1'),
            // ),
            // ElevatedButton(
            //   onPressed: () {
            //     textProvider.changeText('Text #2');
            //   },
            //   child: Text('Text #2'),
            // ),
            // ElevatedButton(
            //   onPressed: () {
            //     textProvider.changeText('Text #3');
            //   },
            //   child: Text('Text #3'),
            // ),
            // ElevatedButton(
            //   onPressed: () {
            //     textProvider.changeText('Text #4');
            //   },
            //   child: Text('Text #4'),
            // ),
          ],
        ),
      ),
    );
  }
}
